import fetch from 'utils/fetch';

export function loginByEmail(mobilePhone,password,loginType) {
  const data = {
    mobilePhone,
    password,
    loginType
  };
  console.log("接收的表单数据"+data);
  return fetch({
    url: '/user/login',
    method: 'post',
    data
  });
}

export function logout() {
  return fetch({
    url: '/user/logout',
    method: 'post'
  });
}

export function getInfo(token) {
  return fetch({
    url: '/user/info',
    method: 'get',
    params: { token }
  });
}

